define([], function () {
    /**
    * authProviders Module
    *
    * Description
    */
    angular.module('authProviders', []).provider('authSettings', [authSettings]);
    function authSettings() {
        this.mainState = 'home';
        this.loginState = 'login';
        this.loginUrl = '/auth/api/v1/auth/';
        this.logoutUrl = '/auth/api/v1/logout/';
        this.checkUrl = '/auth/api/v1/check/';
        this.$get = [function() {
            return {
                mainState: this.mainState,
                loginState: this.loginState,
                loginUrl: this.loginUrl,
                logoutUrl: this.logoutUrl,
                checkUrl: this.checkUrl,
            };
        }];
    }
})
