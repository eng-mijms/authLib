define(['../services/auth_serv','../provider/auth_provider'],
    function () {
        /**
        * authCtrl Module
        *
        * Description
        */
        angular.module('authCtrl', ['authServ', 'ui.router']).controller('userCtrl', ['authServ', '$state', 'authSettings', userCtrl]);
        // debugger

        function userCtrl(authServ, $state, authSettings) {
            // debugger
            us = this;
            us.authenticate = function () {
                authServ.authentication(us.user).then(function(data){
                    // debugger
                    console.log('Authentication Success');
                    $state.go(authSettings.mainState)
                }, function (data) {
                    console.log('authentication error');
                })
            }


            us.logout = function () {
                authServ.logout().then(function(data){
                    $state.go(authSettings.loginState);
                }, function(data){
                    console.log('logout Error');
                })
            }
        }
    });
